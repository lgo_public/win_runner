# Windows runner setup

## Requirements

Windows 10 Pro fresh install.

## Setup

Install [chocolatey](https://chocolatey.org/install).

Open a powershell as administrator.

Install git:

```
choco install git
```

Clone this repository:

```
git clone https://gitlab.com/lgo_public/win_runner.git
```

Go to `win_runner`:

```
cd win_runner
```

Then edit and modify register.ps1:

```
notepad register.ps1
```

Finally, run installation script

```
.\install.ps1
```
