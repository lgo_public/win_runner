$Env:RUNNER_NAME = 'win-runner'
$Env:REGISTRATION_TOKEN = ''
$Env:CI_SERVER_URL = ''
$Env:RUNNER_TAG_LIST = ''

$Env:CONFIG_FILE = "$PSScriptRoot\config.toml"
$Env:REGISTER_RUN_UNTAGGED = 'false'
$Env:REGISTER_LOCKED = 'true'
$Env:RUNNER_EXECUTOR = 'shell'
$Env:RUNNER_SHELL = 'powershell'

$Env:RUNNER_BUILDS_DIR = ''
$Env:RUNNER_CACHE_DIR = ''

gitlab-runner register --non-interactive

(Get-Content config.toml).replace('concurrent = 1', 'concurrent = 2') | Set-Content config.toml