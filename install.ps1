function main
{
  install_dependencies
  define_env
  .\register.ps1
  gitlab-runner install
  gitlab-runner start
}

function install_dependencies
{
  choco install git
  choco install poshgit
  choco install nodejs-lts
  choco install visualstudio2017buildtools
  choco install python2 --params '"/InstallDir:C:\tools\python2"'
  choco install tightvnc
  choco install vscode
  RefreshEnv.cmd
}

function define_env
{
  [System.Environment]::SetEnvironmentVariable('PYTHON', 'C:\tools\python2\python.exe', [System.EnvironmentVariableTarget]::Machine)
  [System.Environment]::SetEnvironmentVariable('GYP_MSVS_VERSION', '2015', [System.EnvironmentVariableTarget]::Machine)
}

main